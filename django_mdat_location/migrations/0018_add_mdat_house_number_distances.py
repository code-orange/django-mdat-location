# Generated by Django 3.2.14 on 2022-08-01 18:07

from django.db import migrations, models
import django.db.models.deletion
import parler.models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_location", "0017_add_google_place_id"),
    ]

    operations = [
        migrations.CreateModel(
            name="MdatHouseNumberDistances",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "drive_distance_km",
                    models.DecimalField(decimal_places=4, max_digits=19),
                ),
                (
                    "from_address",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_location.mdathousenumbers",
                    ),
                ),
                (
                    "to_address",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        related_name="+",
                        to="django_mdat_location.mdathousenumbers",
                    ),
                ),
            ],
            options={
                "db_table": "mdat_house_number_distances",
                "unique_together": {("from_address", "to_address")},
            },
        ),
    ]
