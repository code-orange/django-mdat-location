# Generated by Django 3.2.14 on 2022-08-01 18:45

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_location", "0020_use_meters_instead_of_kilometers_01"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mdathousenumberdistances",
            name="drive_distance_meters",
            field=models.BigIntegerField(),
        ),
    ]
