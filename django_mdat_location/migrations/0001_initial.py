# Generated by Django 3.1.7 on 2021-03-27 15:07
import parler.models
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="MdatCountries",
            fields=[
                ("id", models.BigAutoField(primary_key=True, serialize=False)),
                ("iso", models.CharField(max_length=2, unique=True)),
                ("iso3", models.CharField(max_length=3, null=True, unique=True)),
                ("name", models.CharField(max_length=80)),
                ("printable_name", models.CharField(max_length=80)),
                ("num_code", models.IntegerField(unique=True)),
            ],
            options={
                "db_table": "mdat_countries",
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
    ]
