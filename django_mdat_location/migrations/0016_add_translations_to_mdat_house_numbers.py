# Generated by Django 3.2.13 on 2022-05-14 15:57

from django.db import migrations, models
import django.db.models.deletion
import parler.fields
import parler.models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_location", "0015_add_translations_to_mdat_streets"),
    ]

    operations = [
        migrations.CreateModel(
            name="MdatHouseNumbersTranslation",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language_code",
                    models.CharField(
                        db_index=True, max_length=15, verbose_name="Language"
                    ),
                ),
                (
                    "master",
                    parler.fields.TranslationsForeignKey(
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="translations",
                        to="django_mdat_location.mdathousenumbers",
                    ),
                ),
            ],
            options={
                "verbose_name": "mdat house numbers Translation",
                "db_table": "mdat_house_numbers_translation",
                "db_tablespace": "",
                "managed": True,
                "default_permissions": (),
                "unique_together": {("language_code", "master")},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
    ]
