from dirtyfields import DirtyFieldsMixin
from django.db import models
from django.forms import model_to_dict
from django.utils import timezone
from djgeojson.fields import PointField
from parler.models import TranslatableModel, TranslatedFields


class MdatCountries(TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    iso = models.CharField(unique=True, max_length=2)
    iso3 = models.CharField(unique=True, max_length=3, null=True)
    num_code = models.IntegerField(unique=True, null=True)
    geoloc = PointField(null=True, blank=True)
    google_place_id = models.TextField(null=True)
    date_added = models.DateTimeField(default=timezone.now)

    translations = TranslatedFields(title=models.CharField(max_length=250))

    def __str__(self):
        return self.title

    def to_dict(self):
        data = model_to_dict(self)
        data["title"] = self.title

        return data

    class Meta:
        indexes = [
            models.Index(fields=["iso"]),
            models.Index(fields=["iso3"]),
            models.Index(fields=["num_code"]),
        ]
        db_table = "mdat_countries"


class MdatCities(TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    zip_code = models.CharField(max_length=10)
    country = models.ForeignKey(MdatCountries, models.DO_NOTHING)
    geoloc = PointField(null=True, blank=True)
    google_place_id = models.TextField(null=True)
    date_added = models.DateTimeField(default=timezone.now)

    translations = TranslatedFields(title=models.CharField(max_length=250))

    def __str__(self):
        return "{} {}, {}".format(self.zip_code, self.title, self.country.__str__())

    def to_dict(self):
        data = model_to_dict(self)
        data["title"] = self.title
        data["country"] = self.country.to_dict()

        return data

    class Meta:
        unique_together = (("country", "zip_code"),)
        indexes = [
            models.Index(fields=["country", "zip_code"]),
        ]
        db_table = "mdat_cities"


class MdatStreets(TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    city = models.ForeignKey(MdatCities, models.DO_NOTHING)
    title = models.CharField(max_length=250)
    geoloc = PointField(null=True, blank=True)
    google_place_id = models.TextField(null=True)
    date_added = models.DateTimeField(default=timezone.now)

    translations = TranslatedFields()

    def __str__(self):
        return "{}, {}".format(self.title, self.city.__str__())

    def to_dict(self):
        data = model_to_dict(self)
        data["city"] = self.city.to_dict()

        return data

    class Meta:
        unique_together = (("city", "title"),)
        db_table = "mdat_streets"


class MdatHouseNumbers(TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    street = models.ForeignKey(MdatStreets, models.DO_NOTHING)
    house_nr = models.CharField(max_length=250)
    geoloc = PointField(null=True, blank=True)
    google_place_id = models.TextField(null=True)
    date_added = models.DateTimeField(default=timezone.now)

    translations = TranslatedFields()

    def __str__(self):
        return "{} {}, {}".format(
            self.street.title, self.house_nr, self.street.city.__str__()
        )

    def to_dict(self):
        data = model_to_dict(self)
        data["street"] = self.street.to_dict()

        return data

    def house_nr_split(self):
        # TODO: split house number into numer and chars
        return self.house_nr

    class Meta:
        unique_together = (("street", "house_nr"),)
        db_table = "mdat_house_numbers"


class MdatHouseNumberDistances(DirtyFieldsMixin, models.Model):
    from_address = models.ForeignKey(MdatHouseNumbers, models.DO_NOTHING)
    to_address = models.ForeignKey(
        MdatHouseNumbers, models.DO_NOTHING, related_name="+"
    )

    drive_distance_meters = models.BigIntegerField()
    drive_distance_seconds = models.BigIntegerField()

    date_added = models.DateTimeField(default=timezone.now)
    date_changed = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = timezone.now()

        super(MdatHouseNumberDistances, self).save(*args, **kwargs)

    class Meta:
        unique_together = (("from_address", "to_address"),)
        db_table = "mdat_house_number_distances"
