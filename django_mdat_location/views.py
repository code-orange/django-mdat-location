from dal import autocomplete
from django.db.models import Q
from parler.utils.i18n import get_language

from django_mdat_location.django_mdat_location.models import *


class MdatCountriesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = MdatCountries.objects.all()

        if self.q:
            qs = qs.filter(
                translations__language_code=get_language(),
                translations__title__icontains=self.q,
            )

        return qs


class MdatCitiesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = MdatCities.objects.all()

        country = self.forwarded.get("country", None)

        if country:
            qs = qs.filter(country=country)

        if self.q:
            qs = qs.filter(
                Q(zip_code__icontains=self.q)
                | Q(translations__title__icontains=self.q),
                translations__language_code=get_language(),
            )

        return qs


class MdatStreetsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = MdatStreets.objects.all()

        city = self.forwarded.get("city", None)

        if city:
            qs = qs.filter(city=city)

        if self.q:
            qs = qs.filter(
                title__icontains=self.q,
            )

        return qs


class MdatHouseNumbersAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = MdatHouseNumbers.objects.all()

        street = self.forwarded.get("street", None)

        if street:
            qs = qs.filter(street=street)

        if self.q:
            qs = qs.filter(
                house_nr__icontains=self.q,
            )

        return qs
