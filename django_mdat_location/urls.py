from django.urls import path

from . import views

urlpatterns = [
    path(
        "mdat-countries-autocomplete",
        views.MdatCountriesAutocomplete.as_view(),
        name="mdat-countries-autocomplete",
    ),
    path(
        "mdat-cities-autocomplete",
        views.MdatCitiesAutocomplete.as_view(),
        name="mdat-cities-autocomplete",
    ),
    path(
        "mdat-streets-autocomplete",
        views.MdatStreetsAutocomplete.as_view(),
        name="mdat-streets-autocomplete",
    ),
    path(
        "mdat-house-numbers-autocomplete",
        views.MdatHouseNumbersAutocomplete.as_view(),
        name="mdat-house-numbers-autocomplete",
    ),
]
